#include <stdio.h>
#include "./lib/ll.h"
#include <string.h>
#include <malloc.h>
#include <assert.h>

typedef enum {

	cmd_help,
	cmd_create_list,
	cmd_destroy_list,
	cmd_add_node,
	cmd_remove_node,
	cmd_update_node,
	cmd_search_node,
	cmd_show,
	cmd_quit,
	cmd_unknown

} cmd_id_t;

typedef struct cmd_t{

	cmd_id_t id;
	int argc;
	int (*handler)(struct cmd_t*);
	char* args[];
	
} cmd_t;

ll_t* list = NULL;
char cmd_s[20];

char* help_msg = "Linked list of integers\n  \
Usage:\n \
help : tp see this message\n \
create : to create list\n \
destroy : to remove list from memmory\n \
add [val] [position] : to add new node 0 means beggining e end\n \
remove [pos] : to remove node\n \
update [val] [pos] : to update node\n \
search [val] : to find pos of given value\n"; 
					
cmd_t* cmd_create(cmd_id_t id, int argc, char* args[]);
void cmd_destroy(cmd_t* cmd);
int cmd_process(cmd_t* cmd);
cmd_t* parse_input();
cmd_id_t cmd_id_from_string(char* cmd_s);
int shell();

int main(){
	
	int quit = 0;

	while(!quit){

		quit = shell();
	}	

	return 0;
}

int shell(){

	int quit;

	cmd_t* cmd = parse_input();	
	quit = cmd_process(cmd);
	cmd_destroy(cmd);
			
	return quit;
}

cmd_t* parse_input(){

	char* cmd_name_s;
	char* arg1;
	char* arg2;

	printf("$ ");
	fgets(cmd_s, 20, stdin);
	
	cmd_name_s = cmd_s;
	char* ptr = strtok(cmd_s, " ");
	int argc = 0;
	
	while(ptr != NULL){
		
		
		if (argc == 0)

			cmd_name_s = ptr;
		else if (argc == 1)

			arg1 = ptr;
		else if (argc == 2)
			arg2 = ptr;
		else
			break;
		
		argc++;
		ptr = strtok(NULL, " ");
	}

	strtok(cmd_name_s, "\n");	
	cmd_id_t id = cmd_id_from_string(cmd_name_s);

	if(id == cmd_unknown){

		printf("$ Error. Unknown command see help. \n");
		return NULL;
	}


	char* args[] = {arg1, arg2};

	return cmd_create(id, argc - 1, args);
	
}

int cmd_process(cmd_t* cmd){
	
	if(!cmd)
		return 0;	
		
	return cmd->handler(cmd);
}

cmd_id_t cmd_id_from_string(char* cmd_s){

	static char* cs[] = {"help", "create", "destroy", "add", "remove", "update", "search", "show", "quit"};
	
	for(int i = 0; i <= cmd_quit; i++){
		
		if(strcmp(cs[i], cmd_s) == 0)
			return (cmd_id_t) i;	
	}

	return cmd_unknown;
}


int* create_data_int(int val){

	int* i = (int*) malloc(sizeof(int));

	if(!i)
		return NULL;

	*i = val;
	
	return i;
}

int cmd_proc_help(cmd_t* cmd){
	
	puts(help_msg);
	return 0;
}

int cmd_proc_create(cmd_t* cmd){
		
	if(!list){

		list = ll_create();
		puts("List created.");

	} else 
		puts("List already created.");

	return 0;
}

int cmd_proc_destroy(cmd_t* cmd){

	if(list){
		
		ll_destroy(list);
		list = NULL;
		puts("List destroyed.");
	
	} else {

		puts("List already destroyed");
	}

	return 0;
}

int cmd_proc_add(cmd_t* cmd){

	if(!list){
		
		puts("First create list.");
		return 0;
	
	} else {

		if(cmd->argc != 2){
			
			puts("Not enaugh arguments");
			return 0;		
		}
		
		int data = atoi(cmd->args[0]);
		int pos;

		if(*cmd->args[1] == 'e')
	
			pos = list->len;
		else
			pos = atoi(cmd->args[1]);
	
		int ret = 0;
	
		if(pos > (int) list->len){
			
			printf("Unable to add node at pos %d, becasue list lenght is %zd\n", pos, list->len);
			return 0;
		}

		ret = ll_create_add_at_pos(list, create_data_int(data), sizeof(int), pos);

		if(ret){

			puts("Unable to add - input data problem.");
			
		}	
	}
	
	return 0;
}

int cmd_proc_remove(cmd_t* cmd){

	if(!list){
		
		puts("First create list.");
		return 0;
	
	} else {

		if(cmd->argc != 1){
			
			puts("Not enaugh arguments");
			return 0;		
		}
		
		int pos;

		if(*cmd->args[0] == 'e')
	
			pos = list->len;
		else
			pos = atoi(cmd->args[0]);
	
		int ret = 0;
	
		if(pos > (int) list->len){
			
			printf("Unable to remove at pos %d, becasue list lenght is %zd\n", pos, list->len);
			return 0;
		}
	
		ret = ll_remove_node_at_pos(list, pos);

		if(ret){

			puts("Unable to remove - input data problem.");
			
		}	
	}	
	return 0;
}

int cmd_proc_update(cmd_t* cmd){

	if(!list){
		
		puts("First create list.");
		return 0;
	
	} else {

		if(cmd->argc != 2){
			
			puts("Not enaugh arguments");
			return 0;		
		}
		
		int data = atoi(cmd->args[0]);
		int pos;

		if(*cmd->args[1] == 'e')
	
			pos = list->len;
		else
			pos = atoi(cmd->args[1]);
	
		int ret = 0;
	
		if(pos > (int) list->len){
			
			printf("Unable to update node at pos %d, becasue list lenght is %zd\n", pos, list->len);
			return 0;
		}

		ret = ll_update_node_at_pos(list, create_data_int(data), sizeof(int), pos);

		if(ret){

			puts("Unable to update - input data problem.");
			
		}	
	}

	return 0;
}

int compare(void* v1, void* v2){

	return (*(int*)v1 == *(int*) v2);

}

int cmd_proc_search(cmd_t* cmd){

	if(!list){
		
		puts("First create list.");
		return 0;
	
	} else {

		if(cmd->argc != 1){
			
			puts("Not enaugh arguments");
			return 0;		
		}
		
		int data  = atoi(cmd->args[0]);
		int ret = 0;
	
			
		ret = ll_get_pos_of_node_data(list, &data, compare);
	
		if(ret >= 0)	
			printf("Founded data %d at pos %d\n", data, ret);
		else
			printf("There isn't node with this data.\n");
			
	}	

	return 0;
}

int cmd_proc_show(cmd_t* cmd){

	if(!list){
	
		puts("First create list.");
		return 0;

	}
	
	printf("head->");

	for(int i = 0; i < list->len; i++)
		
		printf("|p:%d v:%d|->", i, *((int*) ll_get_node_at_pos(list, i)->data));		
	
	printf("NULL\n");
	
	return 0;
}

int cmd_proc_quit(cmd_t* cmd){

	ll_destroy(list);
	return 1;
}


cmd_t* cmd_create(cmd_id_t id, int argc, char* args[]){

	static int(*handler[cmd_quit + 1])(cmd_t*) = {
												cmd_proc_help,
												cmd_proc_create,
												cmd_proc_destroy,
												cmd_proc_add,
												cmd_proc_remove,
												cmd_proc_update,
												cmd_proc_search,
												cmd_proc_show,
												cmd_proc_quit};

	assert(id >= 0 && id <= cmd_quit);
	
	cmd_t* cmd = (cmd_t*) malloc(sizeof(cmd_t) + argc * sizeof(*args));

	cmd->id = id;
	cmd->argc = argc;
	
	for(int i = 0; i < argc; i++){
		
		cmd->args[i] = args[i];	
	}

	cmd->handler = handler[id];

	return cmd;
}

void cmd_destroy(cmd_t* cmd){

	free(cmd);

}



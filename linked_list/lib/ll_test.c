#include <stdio.h>
#include "ll.h"
#include <assert.h>
#include <malloc.h>


void disp_ll(ll_t* ll){

	printf("Elements in list: \n");
	for(int i = 0; i< ll->len; i++)
		printf("pos: %d  data: %d\n", i, *((int *)ll_get_node_at_pos(ll, i)->data));
}

int compare_ints(void* val, void* to_comp){

	return (*(int*) val == *(int*) to_comp);
}

int main(){
	
	ll_t* list = ll_create();

	assert(list != NULL);
	
	for (int i = 0; i< 10; i++){

		int* d = (int*) malloc(sizeof(int));
		*d = i;

		int ret = ll_create_add_at_head(list, d, sizeof(int));

		assert(!ret);
		assert(list->len);
		
	}

	printf("Adding to begining...\n");
	disp_ll(list);
	ll_destroy(list);

		
	list = ll_create();

	assert(list != NULL);
	
	for (int i = 0; i< 10; i++){

		int* d = (int*) malloc(sizeof(int));
		*d = i;

		int ret = ll_create_add_at_tail(list, d, sizeof(int));

		assert(!ret);
		assert(list->len);
		
	}

	printf("Adding to end...\n");
	disp_ll(list);

	
	int d = -1;
	printf("Adding new nodes to pos 0, 4, 9, %d\n", d);
	
	int* nd = (int*) malloc(sizeof(int));
	*nd = d;
	ll_create_add_at_pos(list, nd, sizeof(int), 0);
	
	nd = (int*) malloc(sizeof(int));
	*nd = d;
	ll_create_add_at_pos(list, nd, sizeof(int), 4);
	
	nd = (int*) malloc(sizeof(int));
	*nd = d;
	ll_create_add_at_pos(list, nd, sizeof(int), 9);
	
	disp_ll(list);

	printf("Removeing this nodes 0, 3, 7\n");
	
	ll_remove_node_at_pos(list, 0);
	ll_remove_node_at_pos(list, 4 - 1);
	ll_remove_node_at_pos(list, 9 - 2);

	disp_ll(list);

	printf("Update 0, 5, 9 to %d\n", d);

	nd = (int*) malloc(sizeof(int));
	*nd = d;
	ll_update_node_at_pos(list, nd, sizeof(int), 0); 	

	nd = (int*) malloc(sizeof(int));
	*nd = d;
	ll_update_node_at_pos(list, nd, sizeof(int), 5); 	

	nd = (int*) malloc(sizeof(int));
	*nd = d;
	ll_update_node_at_pos(list, nd, sizeof(int), 9); 	

	disp_ll(list);

	printf("Find pos for data = 4\n");

	int dat = 4;
	int pos = ll_get_pos_of_node_data(list, (int* )&dat, compare_ints);

	printf("Found position %d\n", pos);

	ll_destroy(list);
  
	return 0;
}

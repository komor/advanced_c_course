/*
* One direction linked list implementation
* node_t has generic type of data
*/

#ifndef LL_H
#define LL_H
#include <stdlib.h>

typedef struct node_t {
		
	size_t size;
	void* data;
	struct node_t* next;

} node_t;

typedef struct ll_t {

	size_t len;
	int is_empty;
	struct node_t* head;

} ll_t;


ll_t* ll_create();
int ll_destroy(ll_t* ll);
int ll_is_empty(ll_t* ll);
int ll_len(ll_t* ll);

node_t* ll_create_node(ll_t* ll, void* data, size_t size); 
int ll_add_node_at_pos(ll_t* ll, node_t* node, size_t pos);
int ll_add_node_at_head(ll_t* ll, node_t* node);
int ll_add_node_at_tail(ll_t* ll, node_t* node);

int ll_create_add_at_pos(ll_t* ll, void* data, size_t size, size_t pos);
int ll_create_add_at_head(ll_t* ll, void* data, size_t size);
int ll_create_add_at_tail(ll_t* ll, void* data, size_t size);

int ll_remove_node_at_pos(ll_t* ll, size_t pos);
int ll_update_node_at_pos(ll_t* ll, void* data, size_t size, size_t pos);

node_t* ll_get_node_at_pos(ll_t* ll, size_t pos);

int ll_get_pos_of_node_data(ll_t* ll, void* data, int(*compare)(void*  data, void* to_compare));
 
#endif

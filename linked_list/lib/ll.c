#include "ll.h"
#include <assert.h>

ll_t* ll_create(){

	ll_t* ll = (ll_t*) malloc(sizeof(ll_t));

	if(!ll)
		return NULL;
	
	ll->head = NULL;
	ll->is_empty = 1;
	ll->len = 0;

	return ll;
}

int ll_destroy(ll_t* ll){

	if(!ll)
		return 1;

	if(!ll->is_empty)
		while(ll->head != NULL){

			node_t* n = ll->head;
			ll->head = n->next;
				
			if(n->data)
				free(n->data);

			free(n);
		}		
	
	free(ll);

	return 0;
}

int  ll_is_empty(ll_t* ll){

	if(!ll)
		return -1;

	return ll->is_empty;
}

int ll_len(ll_t* ll) {

	if(!ll)
		return -1;

	return ll->len;
} 

node_t* ll_create_node(ll_t* ll, void* data, size_t size){

	if(!ll)
		return NULL;

	if(!data && size > 0)
		return NULL;

	node_t* node = (node_t*) malloc(sizeof(node_t));

	if(!node)
		return NULL;

	node->data = data;
	node->size = size;
	node->next = NULL;

	return node;
}

int ll_add_node_at_pos(ll_t* ll, node_t* node, size_t pos){

	assert(!(!ll || !node || pos < 0));
	if(!ll || !node || pos < 0)
		return 1;

	assert(!(pos > (ll->len)));
	if(pos > (ll->len))
		return 1;

	assert(!(!ll->head && pos != 0));
	if(!ll->head && pos != 0)
		return 1;		

	if(ll->head){
		
		if(!pos){

			node->next = ll->head;
			ll->head = node;

		} else if (pos != ll->len){

			node_t* n = ll_get_node_at_pos(ll, pos - 1);
			assert(n != NULL);		
			if(!n)
				return 1;
		
			node->next = n->next;
			n->next  = node;

		} else {

			node_t* n = ll_get_node_at_pos(ll, pos);
			assert(n != NULL);		
			if(!n)
				return 1;
		
			node->next = n->next;
			n->next = node;			
		}

	} else {

		ll->head = node;
		node->next = NULL;

	}

	ll->len++;
	ll->is_empty = 0;

	return 0;
}

int ll_add_node_at_tail(ll_t* ll, node_t* node){
	
	if(!ll || !node)
		return 1;

	return ll_add_node_at_pos(ll, node, ll->len);
}

int ll_add_node_at_head(ll_t* ll, node_t* node){
	
	
	if(!ll || !node)
		return 1;

	return ll_add_node_at_pos(ll, node, 0);
}

int  ll_create_add_at_pos(ll_t* ll, void* data, size_t size, size_t pos){

	assert(!(!ll || (!data && size > 0)));

	if(!ll || (!data && size > 0))
		return 1;
	
	node_t* node = ll_create_node(ll, data, size);

	assert(node);

	if(!node)
		return 1;

	if(ll_add_node_at_pos(ll, node, pos)){
		
		assert(0);
		
		free(node);	
		return 1;

	} else 
		return 0;
}

int ll_create_add_at_tail(ll_t* ll, void* data, size_t size){

	return ll_create_add_at_pos(ll, data, size, ll->len);
}

int ll_create_add_at_head(ll_t* ll, void* data, size_t size){

	return ll_create_add_at_pos(ll, data, size, 0);
}

int ll_remove_node_at_pos(ll_t* ll, size_t pos){

	if(!ll)
		return 1;
	
	node_t* ncurr = ll_get_node_at_pos(ll, pos);
	
	if(!ncurr)
		return 1;

	if(pos){

		node_t* nprev = ll_get_node_at_pos(ll, pos - 1);

		if(!nprev)
			return 1;
	
		nprev->next = ncurr->next;

	} else {

		ll->head = ncurr->next;
	}
	
	if(ncurr->data)
		free(ncurr->data);
	
	free(ncurr);

	ll->len--;
	
	if(!ll->len)
		ll->is_empty = 1;

	return 0;
}

int ll_update_node_at_pos(ll_t* ll, void* data, size_t size, size_t pos){

	if(!ll)
		return 1;
	
	if(!data && size > 0)
		return 1;

	node_t* node = ll_get_node_at_pos(ll, pos);

	if(!node)
		return 1;

	if(node->data)
		free(node->data);

	node->data = data;
	node->size = size;

	return 0;
}

node_t* ll_get_node_at_pos(ll_t* ll, size_t pos){

	if(!ll)
		return NULL;

	if(!ll->head)
		return NULL;

	node_t* cn = ll->head;
 	size_t cpos = 0;

	while(cn->next != NULL && cpos != pos){

		cn = cn->next;
		cpos++;
	}
	
	return cn;
}

int ll_get_pos_of_node_data(ll_t* ll, void* data, int (*compare)(void*, void*)){

	if(!ll)
		return 1;
	
	if(!data)
		return 1;

	node_t* np = ll->head;
	int pos = 0;
	
	if((*compare)(data, np->data))
			return pos; 
		
	while(np->next != NULL){
		
		pos++;
		np = np->next;
			
		if((*compare)(data, np->data))
			return pos; 
	}
	
	return -1;
}






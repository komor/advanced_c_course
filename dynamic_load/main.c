#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>

#define DLSYM_CHECK if((error = dlerror()) != NULL) {  fputs(error, stderr); exit(1); }

int main(){

	void* str_func_handle = NULL;
	char* error = NULL;

	int (*str_len)(char*);
	int (*char_num_in_string)(char*, char);
	int (*remove_non_alphabets)(char*, char*);
	int (*str_cat)(char*, char*);	
	int (*str_cpy)(char*, char*);
	int (*find_substring)(char*, char*);

	str_func_handle = dlopen("/home/luke/c_wrk/dynamic_load/lib/libstr_func.so", RTLD_LAZY);

	if(!str_func_handle)
		DLSYM_CHECK

	dlerror();
	
	str_len = dlsym(str_func_handle, "str_len");
	DLSYM_CHECK
	
	char_num_in_string = dlsym(str_func_handle, "char_num_in_string");
	DLSYM_CHECK

	remove_non_alphabets = dlsym(str_func_handle, "remove_non_alphabets");
	DLSYM_CHECK
	
	str_cat = dlsym(str_func_handle, "str_cat");
	DLSYM_CHECK

	str_cpy = dlsym(str_func_handle, "str_cpy");
	DLSYM_CHECK

	find_substring = dlsym(str_func_handle, "find_substring");
	DLSYM_CHECK


	char* str = "asdfgasdasdas asfasda 123 333 5";
	char result[100] = {0};
	
	

	printf("Processing %s\n", str);
	printf("len is %d\n", (*str_len)(str));	
	printf("f is %d times\n", (*char_num_in_string)(str, 'f'));
	printf("by removeing %d non alphabetic characters, it becomes %s\n", \
		(*remove_non_alphabets)(result, str), result); 

	char* suffix = "_suffix";
	(*str_cat)(result, suffix);

	printf("cat %s with previous gives %s\n", suffix, result);

	(*str_cpy)(suffix, result);
	printf("copy %s into previous gives %s\n", suffix, result);

	char* sub2 = "nananana";

	printf("substring %s is in previous ?: %d\n", suffix, (*find_substring)(result, suffix));
	
	printf("substring %s is in previous ?: %d\n", sub2, (*find_substring)(result, sub2));

	char* sub3 = "das";

	printf("substring %s is in previous ?: %d\n", sub3, (*find_substring)(result, sub3));

	char* sub4 = "a_suffix";
	printf("substring %s is in previous ?: %d\n", sub4, (*find_substring)(result, sub4));

	dlclose(str_func_handle);
		
	return 0;
}

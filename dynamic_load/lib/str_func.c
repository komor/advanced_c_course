#include "str_func.h"
#include <malloc.h>

#define IS_LOWER_CASE(c) (c <= 0x61 && c >= 0x41) ? 1 : 0
#define IS_UPPER_CASE(c) (c <= 0x7A && c >= 0x41) ? 1 : 0
#define IS_ALPHABET(c)  IS_LOWER_CASE(c) || IS_UPPER_CASE(c) 

int char_num_in_string(char* str, char ch){

	int num = 0; 

	while(*(str++) != 0)
		if(*(str) == ch)
			num++;
	
	return num;
}

int remove_non_alphabets(char* result, char* str){

	int num = 0;
	int len = str_len(str);
	char* temp = (char*) malloc(len);
	
	while(*(str) != 0){
		if (IS_ALPHABET(*str))
			*(temp++) = *(str);
		else
			num++;
		
		str++;
	}
	
	temp -= (len  - num);
	
	while(*(temp) != 0)
		*(result++) = *(temp++);

	*result = 0;

	temp -= len - num;
	free(temp);

	return num;
}


int str_len(char* str){
	
	int len = 0;

	while(*(str++) != 0)
		len++;

	return len;
}

int str_cat(char* str1, char* str2){

	int end_i = str_len(str1);

	while(*(str2) != 0) {
		*(str1 + end_i) = *str2;
		str1++;
		str2++; 
	}

	return 0;
}

int str_cpy(char* source, char* target){

	for(int i = 0; i < str_len(target); i++)
		target[i] = 0;
	
	while(*source != 0)
		*(target++) = *(source++); 
	
	return 0;
}

int find_substring(char* str, char* sub){

	int result = 0;
	int s_i = 0;
	int s_l = str_len(sub);
	char* start = sub;

	while(*(str) != 0){
	
		if(result) {

			if(*str != *sub){

				sub = start;
				s_i = 0;
				result = 0;

			} else {

				s_i++;
				sub++;
				
				if (s_i == s_l){
					break;
				}
			}

		} else {

			if(*str == *sub){

				result = 1;
				s_i++;
				sub++;
			}
		}

		str++;
	}

	return result; 
}

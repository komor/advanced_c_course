
#ifndef STR_FUNC
#define STR_FUNC

/* 
* remove character from string
* @param str string character
* @param ch char to remove from str
* @return int number of removed characters 
*/
int char_num_in_string(char* str, char ch);
int remove_non_alphabets(char* result, char* str);
int str_len(char* str);
// concatenate str2 to str1, 
int str_cat(char* str1, char* str2);
int str_cpy(char* source, char* target);
// 1 when find , 0 when not found
int find_substring(char* str, char* sub);


#endif

#include <stdio.h>
#include "lib/str_func.h"


int main(){

	char* str = "asdfgasdasdas asfasda 123 333 5";
	char result[100] = {0};

	printf("Processing %s\n", str);
	printf("len is %d\n", str_len(str));	
	printf("f is %d times\n", char_num_in_string(str, 'f'));
	printf("by removeing %d non alphabetic characters, it becomes %s\n", \
		remove_non_alphabets(result, str), result); 

	char* suffix = "_suffix";
	str_cat(result, suffix);

	printf("cat %s with previous gives %s\n", suffix, result);

	str_cpy(suffix, result);
	printf("copy %s into previous gives %s\n", suffix, result);

	char* sub2 = "nananana";

	printf("substring %s is in previous ?: %d\n", suffix, find_substring(result, suffix));
	
	printf("substring %s is in previous ?: %d\n", sub2, find_substring(result, sub2));

	char* sub3 = "das";

	printf("substring %s is in previous ?: %d\n", sub3, find_substring(result, sub3));

	char* sub4 = "a_suffix";
	printf("substring %s is in previous ?: %d\n", sub4, find_substring(result, sub4));

		
	return 0;
}

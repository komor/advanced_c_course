#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <stdlib.h>

int main(){

	time_t now; 
	time(&now);
	
	char* time_str = ctime(&now);

	if(!time_str){
		fprintf(stderr, "ctime error %d\n", errno);
		exit(errno);
	}
	printf("Current time is %s", time_str);


	return 0;
}

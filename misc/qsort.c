#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM 50
#define OMIN (double) -0.5
#define OMAX (double) 0.5

#define IMAX  32786
#define IMIN (double) 0 


double scale_range(double omin, double omax, double imin, double imax, double toscale){

	return ((toscale - imin) / (imax - imin)) * (omax - omin) + omin; 
}

void init_rand(){

	srand(time(NULL));
}

double get_rand(double min, double max){

	return scale_range(OMIN, OMAX, IMIN, (double) IMAX, (double) (rand() % IMAX));

}

void fill_rand_array(double* arr, size_t size){

	for(int i = 0; i <  size; i++)
		arr[i] = get_rand(OMIN, OMAX);
}
 
void disp_arr(double* arr, size_t size){

	for(int i = 0; i < size; i++){
	
		double num = arr[i];
		printf("%s%.4f\t", (num > 0) ? " " : "", num);
		
		if(!((i + 1) % 10))
			printf("\n");
	}
	
	printf("\n");
}

int compare(const void* c1, const void* c2){
	
	double c1d = *((double*) c1);
	double c2d = *((double*) c2);

	if(c1d > c2d)
		return 1;
	if(c2d > c1d)
		return -1;
	else 
		return 0;	 
}

int main(){

	double arr[NUM]; 

	init_rand();
	fill_rand_array(arr, NUM);
	
	printf("Unsorted array: \n");
	disp_arr(arr, NUM);

	qsort(arr, NUM,  sizeof(double),  compare);
	
	printf("Sorted array: \n");
	disp_arr(arr, NUM);
		
	return 0;
}

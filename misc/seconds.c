#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>


int main(){
	
	time_t current = time(NULL);
	time_t since_this_month; 

	struct tm* current_data; 
	struct tm since_data; 

	current_data =  localtime(&current);

	since_data = *current_data;

	since_data.tm_mday = 1;
	since_data.tm_hour = 0;
	since_data.tm_min = 0;
	since_data.tm_sec = 0;
	
	time_t since_epoch = mktime(current_data);
	since_this_month = mktime(&since_data);

	int  seconds = difftime(since_epoch, since_this_month);

	printf("%ds elapsed from begginig of this month.\n", seconds);

	return 0;
}

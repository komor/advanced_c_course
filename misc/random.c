#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM 50
#define OMIN (double) -0.5
#define OMAX (double) 0.5

#define IMAX  32786
#define IMIN (double) 0 


double scale_range(double omin, double omax, double imin, double imax, double toscale){

	return ((toscale - imin) / (imax - imin)) * (omax - omin) + omin; 
}

int main(){

	srand(time(NULL));

	printf("Generated %d numbers in range [%.4f , %.4f] : \n", NUM, OMIN, OMAX);

	for(int i = 0; i < NUM; i++){
	
		double num = scale_range(OMIN, OMAX, IMIN, (double) IMAX, (double) (rand() % IMAX));
		printf("%s%.4f\t", (num > 0) ? " " : "", num);
		
		if(!((i + 1) % 10))
			printf("\n");
	}
	
	printf("\n");

	return 0;
}

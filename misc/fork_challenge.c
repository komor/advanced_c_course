#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>

typedef enum {

	parent,
	child, 
	child1, 
	child2, 
	child3,
 	child4
	
} proc_t;

char* map[] = {"parent", "child "};


int get_index(int value, pid_t* pids, int len){

	for(int i = 0; i < len; i++){
		
		if(pids[i] == value)
			return i;
	}
	
	return -1;
}


int main(){

	proc_t type;
	pid_t pid;
	char buff[100] = {0};
	pid_t pids[3];	

	for (int i = 0 ; i < 3; i++){ 
		

		pid = fork();
		pids[i] = pid;
	
		if(pid == 0 )
			type = child;
		else if (pid > 0 )
			type =  parent;
		else {
			fprintf(stderr, "fork failed %d\n", errno);
			return 1;
		}
	}
	
	pid_t p = getpid();
	sprintf(buff, "Process type %s%d pid %d ppid %d\n", map[type], get_index(p, pids, 3), p, getppid());
	write(1, buff, 100);

	return 0;
}

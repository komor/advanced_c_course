#include <stdio.h>

int main(){

	int a = 5;
	int* a_p;
	int** a_pp;

	a_p = &a;
	a_pp = &a_p;

	printf("All posible ways to find value of normla integer variable\n:");
	printf("a = %d\n", a);
	printf("a = *a_p = %d\n", *a_p);
	printf("a = **a_pp = %d\n", **a_pp);
	printf("a = **(&a_p) = %d\n", **(&a_p));
	printf("a = ***(&a_pp) = %d\n", ***(&a_pp));

	printf("All posible ways to find address of the normal integer variable\n");
	printf("&a = %p\n", &a);
	printf("&a = a_p = %p\n", a_p);
	printf("&a = *a_pp = %p\n",* a_pp);
	printf("&a = *(&a_p) = %p\n", *(&a_p));

	return 0;
	
}

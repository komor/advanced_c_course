#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

#define THREADS_NUM 10

int shared_cnt = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
int tasks_finished = 0;

	
void* thread_routine(void* arg){

	pthread_mutex_lock(&mutex);

	if(*(int *) arg % 2 == 0)
		tasks_finished++;
	else
		pthread_cond_wait(&cond, &mutex);
 
	printf("thread id %ld;  arg %d;  global read %d;  ", pthread_self(), *(int*) arg, shared_cnt);
	shared_cnt++;
	printf("global write %d\n", shared_cnt);
	pthread_mutex_unlock(&mutex);

	return 0;

}

int main(){
	
	pthread_t threads[THREADS_NUM];
	int val[THREADS_NUM];

	for(int i = 0; i < THREADS_NUM; i++){
		val[i] = i;
		if(pthread_create(&threads[i], NULL, thread_routine, (void*) &val[i]))
			return 1;
	
	}

	sleep(1);


	while(1){
		if(tasks_finished == THREADS_NUM / 2){
			pthread_cond_broadcast(&cond);
			break;
		}
	}
	for(int i = 0 ; i < THREADS_NUM; i++){

		pthread_join(threads[i], NULL);
		pthread_exit(&threads[i]);

	}

	return 0;
}
